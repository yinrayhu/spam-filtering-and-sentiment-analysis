#!/usr/bin/python3

import sys
import os
import glob

def make_train_file(doc_type):
    # list filenames in "./SENTIMENT_training directory"
    file_pattern = '../SENTIMENT_training/' + doc_type + '.*.txt'
    
    for filename in sorted(glob.glob(file_pattern)):
        with open(filename, 'r', errors = 'ignore') as f:
            in_list = []
            for line in f:
                in_list += line.split()
                # check if an element in in_list contains only one char,
                # if so discard it
                for val in in_list:
                    if len(val) == 1:
                    #and val != '$': <-- this is for SPAM FILTERING!!
                        in_list.remove(val)
            feat_vec = " ".join(in_list)
            
            with open('SENTIMENT_TRAININGFILE_FULL','a') as g:
            # need to write "class label"
                g.write(doc_type + ' ' + feat_vec + '\n')


if __name__ == "__main__":
    make_train_file('POS')
    make_train_file('NEG')
