#!/usr/bin/python3

import sys
import os
import glob

def make_train_file(doc_type, outfile):
    file_pattern = '../SENTIMENT_training/' + doc_type + '.*.txt'
    for filename in glob.glob(file_pattern):
        with open(filename, 'r', errors = 'ignore') as f:
            in_list = []
            for line in f:
                in_list += line.split()
            feat_vec = " ".join(in_list)
        
            with open(outfile,'a') as g:
            # need to write "class label"
                g.write(doc_type + ' ' + feat_vec + '\n')

if __name__ == "__main__":
    argv = list(sys.argv)
    make_train_file('POS', argv[1])
    make_train_file('NEG', argv[1])
