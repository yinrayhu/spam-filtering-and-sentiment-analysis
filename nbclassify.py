# !/usr/bin/python3
# USAGE: python3 nbclassify.py MODELFILE TESTFILE
# INPUT: MODELFILE containing class conditional prob
# INPUT: TESTFILE containing test samples, one per line
# OUTPUT: print classification results to STDOUT, one per line

import sys
import math
import os
import glob
import copy

def read_modelfile(MODELFILE):
    cls_cond_prob = {}
    with open(MODELFILE, 'r') as f:
        for line in f:
            mylist = line.split()
            cls = mylist[0]
            if not cls in cls_cond_prob.keys():
                cls_cond_prob[cls] = {}
            cls_cond_prob[cls][mylist[1]] = float(mylist[2])
        return cls_cond_prob


def cal_likelihood(test_sample, cls_cond_prob):
    # OUTPUT dict: likelihood = {'cls1':prob, 'cls2':prob, ...}
    # test_sample is one line in TESTFILE
    # create likelihood as dict value for each cls
    likelihood = {}
    for cls in cls_cond_prob.keys():
        likelihood[cls] = cls_cond_prob[cls].get('CLASS')
        for word in test_sample.split():
            if word in cls_cond_prob[cls].keys():
                # find word prob given class in cls_cond_prob
                likelihood[cls] += cls_cond_prob[cls].get(word)
            else:
                likelihood[cls] += cls_cond_prob[cls].get('UNK')
    return likelihood
    

def find_maxprob_cls(likelihood):
    # given a dict of {'cls_name':logprob}, find the class w/ max logprob
    for cls, prob in likelihood.items():
        if prob == max(likelihood.values()):
            print(cls, end='\n', file=sys.stdout)


if __name__ == "__main__":
    # 1. class conditional probability is a dict of dict, read from MODELFILE
    #    put class names as keys in cls_cond_prob dict
    # 2. calculate prob(test|class), store as values in likelihood dict
    # 3. use find_maxprob_cls(likelihood) output as classification result
    #    for a single line in TESTFILE
    
    argvlist = list(sys.argv)
    cls_cond_prob = {}
    likelihood = {}
    cls_cond_prob = read_modelfile(argvlist[1])
    
    with open(argvlist[2], 'r') as f:
        for line in f:
            likelihood = cal_likelihood(line, cls_cond_prob)
            find_maxprob_cls(likelihood)

