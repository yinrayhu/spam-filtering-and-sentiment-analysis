#!/usr/bin/python3
"""
USAGE: python3 SENT_get_train_dev.py <partial_trainset> <dev_set_wLabel>
make 80/20 split of all training data into <train, dev> files
"""

import sys
import os
import glob

def make_train_file(doc_type, USED_FILENAMES, trainout):
    # trainout should be argvlist[1]
    # list filenames in "./SENTIMENT_training directory" using glob.glob
    file_pattern = '../SENTIMENT_training/' + doc_type + '.*.txt'
    for numfile, filename in enumerate(glob.glob(file_pattern)):
        USED_FILENAMES.append(filename)
        if numfile == 10000:
            print(doc_type, 'Number of files reached 10k. Stopped processing.')
            return USED_FILENAMES
        else:
            with open(filename, 'r', errors = 'ignore') as f:
                in_list = []
                for line in f:
                    in_list += line.split()
##                    # check if an element in in_list contains only one char,
##                    # if so discard it
##                    for val in in_list:
##                        if len(val) == 1:
##                        #and val != '$': <-- this is for SPAM FILTERING!!
##                            in_list.remove(val)
                feat_vec = " ".join(in_list[1:])
                with open(trainout,'a') as g:
                # need to write "class label"
                    g.write(doc_type + ' ' + feat_vec + '\n')


def make_dev_file(doc_type, USED_FILENAMES, devout):
    file_pattern = '../SENTIMENT_training/' + doc_type + '.*.txt'
    numfile = 0
    for filename in glob.glob(file_pattern):
        if numfile == 2501:
            print(doc_type, 'Number of dev files reached 2.5k. Stopped processing.')
            return USED_FILENAMES
        if filename not in USED_FILENAMES:
            USED_FILENAMES.append(filename)
            numfile += 1
            with open(filename, 'r', errors = 'ignore') as h:
                in_list = []
                for line in h:
                    in_list += line.split()
##                    # check if an element in in_list contains only one char,
##                    # if so discard it
##                    for val in in_list:
##                        if len(val) == 1:
##                        # and val != '$': this is for SPAM filetering!!
##                            in_list.remove(val)
                feat_vec = " ".join(in_list)
        
                with open(devout,'a') as k:
                # need to write "class label"
                    k.write(doc_type + ' ' + feat_vec + '\n')


if __name__ == "__main__":
    USED_FILENAMES = []
    argvlist = list(sys.argv)
    
    make_train_file('POS', USED_FILENAMES, argvlist[1])
    make_train_file('NEG', USED_FILENAMES, argvlist[1])

    make_dev_file('POS', USED_FILENAMES, argvlist[2])
    make_dev_file('NEG', USED_FILENAMES, argvlist[2])

    seen = []
    for filename in USED_FILENAMES:
        if filename not in seen:
            seen.append(filename)
    print('Unique number of files =', len(seen))
    
