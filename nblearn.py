#!/usr/bin/python3

import sys
import os
import math
import copy

def make_doc_count_dict(doc_type, type_count_dict, numdocs):
    # doc_type is the class label, first entry of each line in TRAININGFILE
    # make one dict for doc_type counts:
    # check if doc_type already in type_count_dict,
    # if yes, update <'doct_type', type_count> pair
    # if not, create <'doct_type', 1> in type_count_dict
    if doc_type in type_count_dict:
        type_count_dict[doc_type] += 1
    else:
        type_count_dict[doc_type] = 1
    numdocs += 1
    return type_count_dict, numdocs


def make_wordcount_dict(doc_type, wordslist, word_count_dict, word_stats):
    # word_count_dict -> key is doc_type,
    # value is the dict correspoinding to doc_type
    # create new empty dict for unseen doc_type
    if doc_type not in word_count_dict.keys():
        word_count_dict[doc_type] = {}
    # word_stats_dict is the aggregate stats
    # { <doct_type> : {'total_count':N , 'vocab':k} }
    if doc_type not in word_stats.keys():
        word_stats[doc_type] = {'total_count':0, 'vocab':0}

    # update word_count_dict & word_stats_dict
    for word in wordslist:
        # for every word, total_count always +1
        # for individual word count, needs to check if word in dict
        # for a new word, increase 'vocab' by 1
        word_stats[doc_type]['total_count'] += 1
        if word in word_count_dict[doc_type]:
            word_count_dict[doc_type][word] += 1
        else:
            word_count_dict[doc_type][word] = 1
            word_stats[doc_type]['vocab'] += 1
    return word_count_dict, word_stats


def cal_type_prob(type_count_dict, numdocs):
    type_prob = type_count_dict.copy()
    for doc_type in type_prob.keys():
        type_prob[doc_type] = math.log(type_prob[doc_type] / numdocs)
    return type_prob
#    check type_prob is a correct logprob distribution 
#    print(type_prob)
#    prob = 0
#    for doc_type in type_prob.keys():
#        prob += math.exp(type_prob[doc_type])
#    print(prob)


def cal_wprob_perclass(word_count_dict, word_stats):
    # calculate logprob of word given class with smoothing: p(w|c) = (n+1)/(N+k)
    # where n is the word count given class, N is total #/ words in class
    # k is the vocab size in class
    # also include p(UNK | c) = 1/(N+k)
    word_logprob = copy.deepcopy(word_count_dict)
    
    for doc_type in word_count_dict.keys():
        N = word_stats[doc_type]['total_count']
        k = word_stats[doc_type]['vocab']
        
        for word in word_logprob[doc_type].keys():
            n = word_logprob[doc_type][word]
            #print(word, ' ', n)
            #print((n+1)/(N+k))
            word_logprob[doc_type][word] = math.log((n+1) / (N+k))
        word_logprob[doc_type]['UNK'] = math.log(1/(N+k))

    # NEED TO CHECK word_logprob (wordprob already checked before taking log)
    return word_logprob


def write_modelfile(type_logprob, word_logprob, MODELFILE):
    with open(MODELFILE, 'w') as g:
        for doc_type in word_logprob.keys():
            g.write(doc_type + ' CLASS ' + str(type_logprob[doc_type]) + '\n')
            for word in word_logprob[doc_type].keys():
                g.write(doc_type+' '+word+' '
                        +str(word_logprob[doc_type][word])+'\n')


if __name__ == "__main__":
    type_count_dict = {}
    numdocs = 0
    # word_count_dict is a dict of dictionaries
    word_count_dict = {}
    word_stats = {}
    argvlist = list(sys.argv)
    # print(type(argvlist[1]))
    with open(argvlist[1], 'r') as f:
        for line in f:
            mylist = line.split()
            doc_type = mylist[0]
            type_count_dict, numdocs = \
                             make_doc_count_dict(doc_type, type_count_dict,
                                                 numdocs)
            word_count_dict, word_stats = \
                             make_wordcount_dict(doc_type, mylist[1:],
                                                 word_count_dict, word_stats)

    type_logprob = cal_type_prob(type_count_dict, numdocs)
    word_logprob = cal_wprob_perclass(word_count_dict, word_stats)
    write_modelfile(type_logprob, word_logprob, argvlist[2])
