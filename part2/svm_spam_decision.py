# !/usr/bin/python3
"""
USAGE: python3 svm_spam_decision.py  <*.raw.out>  <*.svm.out>
<*.raw.out> is the output from ./svm_classify
<*.svm.out> is the output file written in the same format as Part I *.out
"""

import sys


def read_raw_from_svm(raw_file):
    # raw_file should be argvlist[1]
    # read floating number output file from ./svm_classify
    # make decision based on > 0 say 'HAM'
    decision = []
    with open(raw_file, 'r') as f:
        for num in f:
            if float(num) > 0:
                decision.append('HAM')
            else:
                decision.append('SPAM')
    #print(decision)
    return decision


def write_svm_out(svm_out, decision):
    # svm_out should be argvlist[2]
    with open(svm_out, 'a') as f:
        for i in decision:
            f.write(i + '\n')


if __name__ == "__main__":
    argvlist = list(sys.argv)
    decision = read_raw_from_svm(argvlist[1])
    write_svm_out(argvlist[2], decision)
