#!/usr/bin/python3

import sys
import os
import glob

def make_train_file(doc_type):
    # list filenames in "./SPAM_training directory"
    file_pattern = '../../SPAM_training/' + doc_type + '.*.txt'
    for filename in sorted(glob.glob(file_pattern)):
        with open(filename, 'r', errors = 'ignore') as f:
            in_list = []
            for line in f:
                in_list += line.split()
            feat_vec = " ".join(in_list)
        
            with open('svm_SPAM_TRAININGFILE','a') as g:
            # need to write "class label"
                g.write(doc_type + ' ' + feat_vec + '\n')


def make_dev_wLabel(doc_type):
    # list filenames in "./SPAM_training directory"
    file_pattern = '../../SPAM_dev/' + doc_type + '.*.txt'
    for filename in sorted(glob.glob(file_pattern)):
        with open(filename, 'r', errors = 'ignore') as f:
            in_list = []
            for line in f:
                in_list += line.split()
            feat_vec = " ".join(in_list)
        
            with open('svm_SPAM_DEV_wLabel','a') as g:
            # need to write "class label"
                g.write(doc_type + ' ' + feat_vec + '\n')


if __name__ == "__main__":
    make_train_file('HAM')
    make_train_file('SPAM')

    make_dev_wLabel('HAM')
    make_dev_wLabel('SPAM')
