# !/usr/bin/python3
# USAGE: python3 svm_make_SENTIMENT_feat.py <TRAININGFILE> <*.wid> <*.feat>
# assign word_id to unique words in TRAININGFILE,
# write word:word_id to *.wid file for postprocessing
# make *.feat using word_id and counts

import sys
import os
import math
import copy


def create_wid(argvlist):
    # assign word_id, write to SENTIMENT_full.wid file
    word_id_dict = {}
    ind = 1
    with open(argvlist[1], 'r') as f:
        for line in f:
            mylist = line.split()
            for word in mylist[1:]:
                if word not in word_id_dict.keys():
                    # make word id itself also a string, instead of a num
                    word_id_dict[word] = ind
                    ind += 1
    with open(argvlist[2], 'a') as g:
        for word in word_id_dict.keys():
            g.write(word + ' ' + str(word_id_dict[word]) + '\n')
    print(str(ind-1) + ' unique words')
    return word_id_dict


def write_feat_val_pair(argvlist, word_id_dict, cls_id):
    # convert word in each doc in TRAININGFILE to word_id:count pair
    with open(argvlist[1], 'r') as f:
        for line in f:
            # 'feat_vec' is the result for one line in svm TRAININGFILE
            # word_id_val dict = {'word_id':word_count_in_line}
            # word_id_val should be sorted by ascending word_id
            # before being merged into 'feat_vec'
            feat_vec = ''
            word_id_val_dict = {}
            word_id_val_list = []
            mylist = line.split()

            # if word had wid, count its apperances in line
            for word in mylist[1:]:
                if word in word_id_dict.keys():
                    # wid is a num, which is immutable, can be dict key
                    wid = word_id_dict.get(word)
                    word_id_val_dict[wid] = mylist[1:].count(word)

            # make word_id_val_dict into a list and then sort it
            # call word_id_val_list
            for k in sorted(word_id_val_dict.keys()):
                count = word_id_val_dict[k]
                mystr = ':'.join([str(k), str(count)])
                word_id_val_list.append(mystr)
                
            feat_vec = ' '.join(word_id_val_list)
            feat_vec = cls_id[mylist[0]] + ' ' + feat_vec
            #print(feat_vec)
            with open(argvlist[3], 'a') as g:
                g.write(feat_vec + '\n')
            

if __name__ == "__main__":
    # word_id_dict is a dict of dictionaries, assign word_id
    # to training set unique words
    # word_id_dict = { 'word':'id', ...}
    word_id_dict = {}
    argvlist = list(sys.argv)
    
    # for SENTIMENT:
    cls_id = {'POS':'1', 'NEG':'-1'}
    
    word_id_dict = create_wid(argvlist)
    write_feat_val_pair(argvlist, word_id_dict, cls_id)
