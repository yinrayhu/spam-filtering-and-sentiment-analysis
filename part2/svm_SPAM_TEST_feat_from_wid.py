# !/usr/bin/python3
# USAGE: python3 svm_TEST_feat_from_wid.py <TESTFILE> <*.wid> <*.feat>
# take word_id in <*.wid>, process <TESTFILE>, save as <*.feat>
# output class label all "1"

import sys
import os
import math


def read_wid(argvlist):
    # read *.id file, make word_id_dict
    word_id_dict = {}
    with open(argvlist[2], 'r') as f:
        for line in f:
            mylist = line.split()
            word_id_dict[mylist[0]] = int(mylist[1])
    return word_id_dict


def write_feat_val_pair(argvlist, word_id_dict):
    # convert word in each doc in TESTFILE to word_id:count pair
    with open(argvlist[1], 'r') as f:
        for line in f:
            # 'feat_vec' is the result for one line in svm TESTFILE
            # word_id_val dict = {'word_id':word_count_in_line}
            # word_id_val should be sorted by ascending word_id
            # before being merged into 'feat_vec'
            feat_vec = ''
            word_id_val_dict = {}
            word_id_val_list = []
            mylist = line.split()

            # if word had wid, count its apperances in line
            for word in mylist:
                if word in word_id_dict.keys():
                    # wid is a num, which is immutable, can be dict key
                    wid = word_id_dict.get(word)
                    word_id_val_dict[wid] = mylist.count(word)

            # make word_id_val_dict into a list and then sort it
            # call word_id_val_list
            for k in sorted(word_id_val_dict.keys()):
                count = word_id_val_dict[k]
                mystr = ':'.join([str(k), str(count)])
                word_id_val_list.append(mystr)
                
            feat_vec = ' '.join(word_id_val_list)
            feat_vec = '1 ' + feat_vec
            #print(feat_vec)
            with open(argvlist[3], 'a') as g:
                g.write(feat_vec + '\n')
            

if __name__ == "__main__":
    # word_id_dict is a dict of dictionaries, assign word_id
    # to training set unique words
    # word_id_dict = { 'word':'id', ...}
    word_id_dict = {}
    argvlist = list(sys.argv)
    
    word_id_dict = read_wid(argvlist)
    write_feat_val_pair(argvlist, word_id_dict)
