# !/usr/bin/python3
# USAGE: python3 f-score-per-label.py <correct_label> <classify_output> <class>
# calculate F-score: 
# read correct labels into a list, do the same for classified results
# loop through the list and calculate TP, TN, FP, FN
# PRECISION = TP / (TP + FP); RECALL = TP / (TP + FN)
# specify <class> as the label to calculate precision and recall for

import os
import sys


def cal_f_score(labelfile, resultfile, pos_cls):
    label = []
    result = []
    TP = 0
    TN = 0
    FP = 0
    FN = 0
    pre = 0
    rec = 0
    scr = 0
    with open(labelfile, 'r') as f:
        for line in f:
            label.append(line.strip())
    with open(resultfile, 'r') as g:
        for line in g:
            result.append(line.strip())
    for i, lab in enumerate(label):
##        print(lab)
##        print(result[i])
##        print('next test:')
        if lab == pos_cls:
            if result[i] == pos_cls:
                TP += 1
        if lab != pos_cls and result[i] != pos_cls:
            TN += 1
        if lab != pos_cls and result[i] == pos_cls:
            FP += 1
        if lab == pos_cls and result[i] != pos_cls:
            FN += 1
    #print('[TP, TN, FP, FN] =',[TP, TN, FP, FN])
    print('Label: '+ pos_cls)
    pre = TP / (TP + FP)
    rec = TP / (TP + FN)
    #print('[pre, rec] = ', [pre, rec])
    scr = 2*pre*rec / (pre+rec)
    return pre, rec, scr

    
if __name__ == "__main__":
    # pos_cls is the "positive" class for "true positive" in detective
    argvlist = list(sys.argv)
    correct_label = argvlist[1]
    classify_result = argvlist[2]
    pos_cls = argvlist[3]
    
    pre, rec, scr = cal_f_score(correct_label, classify_result, pos_cls)

    print('Precision =', pre)
    print('Recall =', rec)
    print('F-score =', scr)
