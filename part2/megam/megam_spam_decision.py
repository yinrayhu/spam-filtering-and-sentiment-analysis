# !/usr/bin/python3
"""
USAGE: python3 megam_spam_decision.py  <*.rawout>  <*.megam.out>
<*.rawout> is the output from ./megam -predict
<*.megam.out> is the output file written in the same format as Part I *.out
"""

import sys


def read_raw_from_megam(raw_file):
    # raw_file should be argvlist[1]
    # read floating number output file 
    # make decision based on > 0.5 say 'HAM'
    decision = []
    with open(raw_file, 'r') as f:
        for line in f:
            mylist = line.split()
            num = float(mylist[1])
            if num > 0.5:
                decision.append('HAM')
            else:
                decision.append('SPAM')
    #print(decision)
    return decision


def write_megam_out(megam_out, decision):
    # svm_out should be argvlist[2]
    with open(megam_out, 'a') as f:
        for i in decision:
            f.write(i + '\n')


if __name__ == "__main__":
    argvlist = list(sys.argv)
    decision = read_raw_from_megam(argvlist[1])
    write_megam_out(argvlist[2], decision)
