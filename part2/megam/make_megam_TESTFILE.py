# !/usr/bin/python3
"""
USAGE:  python3  make_megam_TESTFILE.py  TESTFILE  TESTFILE.megam
This script works for both SPAM and SENTIMENT
1. remove length 1 words
2. insert 1 at the start of each line
"""

import sys


def check_len(inline):
    outline = ['1']
    for word in inline[1:]:
        if len(word) > 1:
            # skip len(word) = 1 case
            outline.append(word) 
    return outline


def main():
    argvlist = list(sys.argv)    
    with open(argvlist[1], 'r') as f, open(argvlist[2], 'a') as g:
        for line in f:
            outline = check_len(line.split())
            #print(outline)
            g.write(' '.join(outline) + '\n')


if __name__ == "__main__":
    main()
