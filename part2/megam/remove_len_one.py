# !/usr/bin/python3
"""
USAGE:  python3  remove_len_one.py  <INFILE>  <OUTFILE>
Check from second column of <INFILE>, remove length one char.
Ignore first column which is the class label.
This is for megam use.
"""

import sys
import copy


def check_len(inline):
    # don't check for the first col, leave as is since it's a class label
    outline = [inline[0]]
    for word in inline[1:]:
        if len(word) > 1:
            # skip len(word) = 1 case
            outline.append(word) 
    return outline


def main():
    argvlist = list(sys.argv)    
    with open(argvlist[1], 'r') as f, open(argvlist[2], 'a') as g:
        for line in f:
            outline = check_len(line.split())
            #print(outline)
            g.write(' '.join(outline) + '\n')


if __name__ == "__main__":
    main()
