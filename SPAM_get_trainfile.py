#!/usr/bin/python3

import sys
import os
import glob

def make_train_file(doc_type):
    # list filenames in "./SPAM_training directory"
    file_pattern = '../SPAM_training/' + doc_type + '.*.txt'
    for filename in glob.glob(file_pattern):
        with open(filename, 'r', errors = 'ignore') as f:
            in_list = []
            for line in f:
                in_list += line.lower().split()
            feat_vec = " ".join(in_list)
        
            with open('training_file','a') as g:
            # need to write "class label"
                g.write(doc_type + ' ' + feat_vec + '\n')

if __name__ == "__main__":
    make_train_file('HAM')
    make_train_file('SPAM')
