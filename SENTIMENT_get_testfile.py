#!/usr/bin/python3

import sys
import os
import glob

# use the following to make sure the correct TESTFILE order
# for filename in sorted(glob.glob('*.py')):
# 	<do sth>
	
def make_test_file(outfile):
    for filename in sorted(glob.glob('../SENTIMENT_test/*.txt')):
        with open(filename, 'r', errors = 'ignore') as f:
            in_list = []
            for line in f:
                in_list += line.split()
            feat_vec = " ".join(in_list)
        
            with open(outfile,'a') as g:
            # need to cut "class label", do it in shell cut -f2-
                g.write(feat_vec + '\n')

if __name__ == "__main__":
    outfile = sys.argv[1]
    make_test_file(outfile)
