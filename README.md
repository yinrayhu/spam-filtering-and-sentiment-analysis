use the following script in part2/ folder to calculate F-score:
USAGE: python3 f-score-per-label.py <correct_label> <classify_output> <class>

-- What are the precision, recall and F-score on the development data for your classifier in part I for each of the two datasets. Report precision, recall and F-score for each label.

Ans: 

For SPAM set:

- label HAM
Precision = 0.9919110212335692
Recall = 0.981
F-score = 0.9864253393665159

- label SPAM
Precision = 0.9491978609625669
Recall = 0.977961432506887
F-score = 0.9633649932157395

For Sentiment set:

- Label: POS
Precision = 0.87641427328111
Recall = 0.80592236894758
F-score = 0.83969147383782

- Label: NEG
Precision = 0.8203703703703704
Recall = 0.886354541816726
F-score = 0.8520869397961146

*********************************************************************
- What are the precision, recall and F-score for your classifier in part II for each of the two datasets. Report precision, recall and F-score for each label.

Ans:

- SVM:

For SPAM set:

- Label: HAM
Precision = 0.993933265925177
Recall = 0.9256120527306968
F-score = 0.9585568015602145

- Label: SPAM
Precision = 0.7887700534759359
Recall = 0.9800664451827242
F-score = 0.8740740740740742

For SENTIMENT set:

- Label: POS
Precision = 0.8394301116673084
Recall = 0.8723489395758304
F-score = 0.8555729984301412

- Label: NEG
Precision = 0.8671386922115785
Recall = 0.8331332533013205
F-score = 0.8497959183673468


- MegaM:

For SPAM set:

Label: HAM
Precision = 0.99000999000999
Recall = 0.991
F-score = 0.9905047476261869
 
Label: SPAM
Precision = 0.9751381215469613
Recall = 0.9724517906336089
F-score = 0.9737931034482759 

For Sentiment set:

Label: POS
Precision = 0.9167652859960552
Recall = 0.9299719887955182
F-score = 0.9233214143822011

Label: NEG
Precision = 0.928948436865611
Recall = 0.9155662264905963
F-score = 0.9222087867795243

- What happens exactly to precision, recall and F-score in each of the two tasks (on the development data) when only 10% of the training data is used to train the classifiers in part I and part II? Why do you think that is?
Ans: Because training data is scarce, the classifier performance suffer greatly.

